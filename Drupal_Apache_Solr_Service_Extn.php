<?php

require_once (drupal_get_path('module', 'apachesolr') .'/SolrPhpClient/Apache/Solr/Service.php');
require_once (drupal_get_path('module', 'apachesolr') .'/Drupal_Apache_Solr_Service.php');


/**
 * This is extension of Drupal_Apache_Solr_Service class which prevents the POST operation.
 * 
 *
 */

class Drupal_Apache_Solr_Service_Extn extends Drupal_Apache_Solr_Service {

/**
  * Central method for making a post operation against this Solr Server
  *
  * @see Apache_Solr_Service::_sendRawPost()
  */
protected function _sendRawPost($url, $rawPost, $timeout = FALSE, $contentType = 'text/xml; charset=UTF-8') {
//POST is needed for search query greater than 4000 chars
if(strstr($url, '/select')){
return parent::_sendRawPost($url, $rawPost,FALSE, 'application/x-www-form-urlencoded');
}
if (variable_get('apachesolr_read_only', 0)) {
    throw new Exception('Operating in read-only mode; updates are disabled.');
  }

  $request_headers = array('Content-Type' => $contentType);
  $status = $this->write_xml($url, $request_headers, $rawPost);
  return true;
}

/**
 * Add an array of Solr Documents to the index all at once
 *
 * @param array $documents Should be an array of Apache_Solr_Document instances
 * @param boolean $allowDups
 * @param boolean $overwritePending
 * @param boolean $overwriteCommitted
 * @return Apache_Solr_Response
 *
 * @throws Exception If an error occurs during the service call
 */
public function addDocuments($documents, $allowDups = false, $overwritePending = true, $overwriteCommitted = true)
{
  foreach ($documents as $document) {
    if ($document instanceof Apache_Solr_Document) {
	$rawPost .= $this->_documentToXmlFragment($document);
  }
  }
  return $this->add($rawPost);
}

public function _merge($rawPost, $allowDups = false, $overwritePending = true, $overwriteCommitted = true) {
  $dupValue = $allowDups ? 'true' : 'false';
  $pendingValue = $overwritePending ? 'true' : 'false';
  $committedValue = $overwriteCommitted ? 'true' : 'false';
  $rawPostPrepend = '<add allowDups="' . $dupValue . '" overwritePending="' . $pendingValue . '" overwriteCommitted="' . $committedValue . '">';
  $rawPostAppend = '</add>';
  $xml_dir = variable_get('apachesolr_update_path', '/tmp/');
  $this->truncate_file();
  $xml_string = file_get_contents($xml_dir .'/solr.xml');
  if( ($xml_string == '') && !empty($rawPost)) {
    $rawPost = $rawPostPrepend . $rawPost . $rawPostAppend;
  }
  else {
    $temp =  substr($xml_string, 0, (strlen($xml_string) - 6));
    $rawPost = $temp.$rawPost.$rawPostAppend;
  }
  return $rawPost;
}

/**
 * Raw Add Method. Takes a raw post body and sends it to the update service.  Post body
 * should be a complete and well formed "add" xml document.
 *
 * @param string $rawPost
 * @return Apache_Solr_Response
 *
 * @throws Exception If an error occurs during the service call
 */
public function add($rawPost) {
  return $this->_sendRawPost($this->_updateUrl, $this->_merge($rawPost));
}

//    Empty the file at the beginning of a cron run
public function truncate_file() {
  $xml_dir = variable_get('apachesolr_update_path', '/tmp/');
  static $clear_xml_file;
  if( !isset($clear_xml_file)) {
    file_put_contents($xml_dir .'/solr.xml', '');
    $clear_xml_file = 0;
  }
}

public function write_xml($url, $request_headers, $raw_post) {
  $this->truncate_file();
  $xml_dir = variable_get('apachesolr_update_path', '/tmp/');
  //Use file_put_contents as fopen is not permitted for https based sites
  if( file_put_contents($xml_dir .'/solr.xml', $raw_post))
  return TRUE;
  
  return FALSE;
}
}
